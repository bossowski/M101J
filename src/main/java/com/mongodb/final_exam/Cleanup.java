package com.mongodb.final_exam;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import static com.mongodb.client.model.Filters.eq;

public class Cleanup {

    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase database = client.getDatabase("test");

        int orphanImageCount = 0;

        MongoCollection<Document> albumsCol = database.getCollection("albums");
        MongoCollection<Document> imagesCol = database.getCollection("images");
        List<Document> images = imagesCol.find().into(new ArrayList<Document>());
        for (Document image : images) {
            int imageId = image.getInteger("_id");

            Document album = albumsCol.find(Filters.eq("images", imageId)).first();
            if (album == null) {
                imagesCol.deleteOne(Filters.eq("_id", imageId));
                orphanImageCount++;
            }
        }

        System.out.println("Removed images count: " + orphanImageCount);
    }
}
