package com.mongodb.week_5;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Created by bossowski on 2016-03-28.
 */
public class ZipCodeAggregationTest{
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase database = client.getDatabase("test");
        MongoCollection<Document> collection = database.getCollection("zips");

        List<Bson> pipeline = asList(Aggregates.group("$state", Accumulators.sum("totalPop", "$pop")), Aggregates.match(Filters.gte("totalPop", 1000000)) );
        List<Document> results = collection.aggregate(pipeline).into(new ArrayList<Document>());

        for (Document cur : results) {
            System.out.println(cur.toJson());
        }
    }
}
