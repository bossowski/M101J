package com.mongodb.week_3;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by bossowski on 2016-04-05.
 */
public class Homework_3_1 {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase database = client.getDatabase("school");
        MongoCollection<Document> collection = database.getCollection("students");

        List<Document> all = collection.find().into(new ArrayList<Document>());
        for (Document doc : all) {
            System.out.println(doc);
            List<Document> scores = doc.get("scores", List.class);

            Double lowest = null;
            Document lowestDoc = null;
            for (Document score : scores) {
                if ("homework".equals(score.getString("type"))) {
                    double scoreValue = score.getDouble("score");
                    System.out.println(scoreValue);

                    if(lowest == null || lowest.doubleValue() > scoreValue){
                        lowest = scoreValue;
                        lowestDoc = score;
                    }
                }
            }

            System.out.println("lowest score: " + lowest);
            scores.remove(lowestDoc);
            System.out.println(scores);

            Document update = new Document("$set", new Document("scores", scores));
            collection.updateOne(eq("_id", doc.getInteger("_id")), update);
//            collection.updateOne(eq("_id", doc.getInteger("_id")), Updates.set("scores", scores));
        }
    }
}
