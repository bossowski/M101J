package com.mongodb.week_2;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.orderBy;

/**
 * Created by bossowski on 2016-03-28.
 */
public class Homework_2_3 {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase database = client.getDatabase("students");
        MongoCollection<Document> collection = database.getCollection("grades");

        Bson filter =  eq("type", "homework");
        Bson sort = orderBy(ascending("student_id"), ascending("score"));

        // db.grades.find({type:"homework"}).sort({"student_id":1,'score':1})
        List<Document> all = collection.find(filter).sort(sort).into(new ArrayList<Document>());

        Integer previousStudentId = null;
        for (Document cur : all) {
            int studentId = cur.getInteger("student_id");
            if(previousStudentId == null || previousStudentId != studentId) {
                System.out.println("Deleting: {" + studentId  +"} with score: {" + cur.getDouble("score") + "}" );
                collection.deleteOne(eq("_id", cur.getObjectId("_id")));
                previousStudentId = studentId;
            }
            else{
                System.out.println("Skipping: {" + studentId  +"} with score: {" + cur.getDouble("score") + "}" );
            }
        }
    }
}
